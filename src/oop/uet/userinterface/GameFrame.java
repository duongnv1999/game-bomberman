/**
 * game Bomberman
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.userinterface;

import javax.swing.*;
import java.awt.*;


public class GameFrame extends JFrame {
    public static final int SCREEN_HEIGHT = 550;
    public static final int SCREEN_WIDTH = 800;
    GamePanel gamePanel;

    public GameFrame() {
        super("BomberMan");

        Toolkit toolkit = this.getToolkit();
        Dimension dimension = toolkit.getScreenSize(); // lay kich thuoc cua man hinh
        this.setBounds((int)(dimension.getWidth()-SCREEN_WIDTH)/2, (int)(dimension.getHeight()- SCREEN_HEIGHT)/2, SCREEN_WIDTH, SCREEN_HEIGHT); // dat vi tri giua man hinh khi game hien ra
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // khi tat bang colse thi chuong trinh cung dung.
        gamePanel = new GamePanel();
        add(gamePanel);
        addKeyListener(gamePanel);
    }

    public void startGame() {
        gamePanel.startGame();
        this.setVisible(true);
    }

}
