/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.userinterface;

import oop.uet.GameObject.GameWorld;

import java.awt.event.KeyEvent;

/**
 * class nay la noi xu li khi nguoi dung an vao cac nut up, down, left, right, va space
 */

public class InputManager {

    GameWorld gameWorld;

    public InputManager(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public void processKeyPressed(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
            {
                gameWorld.player.setKEY_UP(true);
                break;
            }
            case KeyEvent.VK_DOWN :
            {
                gameWorld.player.setKEY_DOWN(true);
                break;
            }
            case KeyEvent.VK_LEFT :
            {
                gameWorld.player.setKEY_LEFT(true);
                break;
            }
            case KeyEvent.VK_RIGHT :
            {

                gameWorld.player.setKEY_RIGHT(true);
                break;
            }
            case KeyEvent.VK_SPACE :
            {
                gameWorld.player.setKEY_SPACE(true);
                break;
            }

        }
    }

    public void processKeyReleased(int keyCode) {
        switch (keyCode) {
            case KeyEvent.VK_UP:
            {
                gameWorld.player.setKEY_UP(false);
                break;
            }
            case KeyEvent.VK_DOWN :
            {
                gameWorld.player.setKEY_DOWN(false);
                break;
            }
            case KeyEvent.VK_LEFT :
            {
                gameWorld.player.setKEY_LEFT(false);
                break;
            }
            case KeyEvent.VK_RIGHT :
            {
                gameWorld.player.setKEY_RIGHT(false);
                break;
            }
            case KeyEvent.VK_SPACE :
            {
                gameWorld.player.setKEY_SPACE(false);
                break;
            }

        }
    }
}
