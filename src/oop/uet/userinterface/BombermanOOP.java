/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.userinterface;

public class BombermanOOP {
    public static void main(String args[]) {
        GameFrame gameFrame = new GameFrame();
        gameFrame.startGame();
    }
}
