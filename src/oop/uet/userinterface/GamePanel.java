/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.userinterface;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.CacheDataLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

public class GamePanel extends JPanel implements Runnable, KeyListener {

    private Thread thread;
    private boolean IsRunning = true;
    private InputManager inputManager;
    private BufferedImage nextFrameImage;
    private Graphics2D nextFrameG2D;

    public GameWorld gameWorld;



    //constructor mac dinh
    public GamePanel() {
        CacheDataLoader.getInstance().loadData();
        gameWorld = new GameWorld();
        inputManager = new InputManager(gameWorld);
    }


    @Override
    public void paint(Graphics g) {
        g.drawImage(gameWorld.getBufferedImage(), 0, 0, this);
    }


    //tao thread chay game
    public void startGame() {
            thread = new Thread(this);
            thread.start();
    }

    @Override
    public void run() {
        final int FPS = 60;
        long period = 1000*1000000/FPS; // chu ki cua 1 lan Update va Render
        long beginTime;
        long sleepTime;

        beginTime = System.nanoTime();
        while (IsRunning) {

            gameWorld.update();
            gameWorld.render();
            repaint();

            long deltaTime = System.nanoTime()- beginTime;
            sleepTime = period - deltaTime;

            try {
                if (sleepTime > 0) {
                    thread.sleep(sleepTime/2000000);
                } else {
                    thread.sleep(period/2000000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            beginTime = System.nanoTime();
        }
    }

    //
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        inputManager.processKeyPressed(e.getKeyCode());

    }

    @Override
    public void keyReleased(KeyEvent e) {
        inputManager.processKeyReleased(e.getKeyCode());
    }
}
