/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject;

import java.awt.*;

public abstract class GameObject {
    private float posX;
    private float posY;
    private GameWorld gameWorld;
    private String type;

    public GameObject(float posX, float posY, GameWorld gameWorld) {
        this.posX = posX;
        this.posY = posY;
        this.gameWorld = gameWorld;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public String getType(){
        return type;
    }
    public void setType (String type) {
        this.type = type;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public abstract void update();

    public abstract void draw(Graphics2D g2);

    public Rectangle getBoundForCollision(){return null;}


}
