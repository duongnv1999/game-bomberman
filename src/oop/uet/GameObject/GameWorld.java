/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject;

import oop.uet.GameObject.Map.PhysicalMap;
import oop.uet.GameObject.ParticularObject.GameObjectManager;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.SpeedItem;
import oop.uet.GameObject.ParticularObject.MovableObject.Player;
import oop.uet.userinterface.GameFrame;

import javax.imageio.ImageIO;
import java.awt.Color;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * class nay la noi chua toan bo cac doi tuong trong game, va co chuc nang update tung doi tuong, ve tat cac cac doi tuong ra 1 image de in ra gamepanel
 * xu li khi ket thuc game, khi bat dau game, va xu li cac level
 */

public class GameWorld {

    public Player player;
    public static PhysicalMap physicalMap;
    public Camera camera;
    public SpeedItem speedItem;
    public GameObjectManager gameObjectManager;
    private int numberOfTries;
    private boolean backToCheckPoint;
    long timeToDrawScreenAfterDead;
    boolean passLevel;
    Graphics2D g2;

    private BufferedImage bufferedImage = new BufferedImage(GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT, BufferedImage.TYPE_INT_ARGB);

    public GameWorld() {
        gameObjectManager = new GameObjectManager(this);
        physicalMap = new PhysicalMap(0, 0, this);
        physicalMap.loadEnemyAndBrick();
        physicalMap.loadItem();
        player = new Player(PhysicalMap.PLAYER_X, PhysicalMap.PLAYER_Y, 30, 30, this);
        camera = new Camera(0, 0, GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT, this);
        speedItem = new SpeedItem(100, 100, 30, 30, this);
        g2 = (Graphics2D) bufferedImage.getGraphics();
        setNumberOfTries(3);
        passLevel = false;
    }


    public int getNumberOfTries() {
        return numberOfTries;
    }

    public void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    public boolean isBackToCheckPoint() {
        return backToCheckPoint;
    }

    public void setBackToCheckPoint(boolean backToCheckPoint, long timeToDrawScreenAfterDead) {
        this.backToCheckPoint = backToCheckPoint;
        this.timeToDrawScreenAfterDead = timeToDrawScreenAfterDead;
    }

    public boolean isPassLevel() {
        return passLevel;
    }
    public void setPassLevel(boolean passLevel) {
        this.passLevel = passLevel;
    }

    public void drawGameOver() {
        g2.setColor(Color.DARK_GRAY);
        g2.fillRect(0,0, GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT);
        try {
            BufferedImage level1Image = ImageIO.read(new File("Data/textures/gameover.png"));
            g2.drawImage(level1Image, GameFrame.SCREEN_WIDTH/2 - level1Image.getWidth()/2, GameFrame.SCREEN_HEIGHT/2 - level1Image.getHeight()/2, null);
        } catch (Exception e) {}
    }

    public void update() {
        if (!passLevel) {
            if (numberOfTries > 0) {
                if (backToCheckPoint) {
                    if (System.nanoTime() - timeToDrawScreenAfterDead <= 10 * 1000000000) {
                        drawScreenBeforeNewTry();
                    } else {
                        loadNewDataBeforeNewTry();
                        setBackToCheckPoint(false, System.nanoTime());
                    }
                } else {
                    player.update();
                    gameObjectManager.updateAllObject();
                    camera.update();
                }
            } else {
                drawGameOver();
            }
        }
    }

    public void drawScreenBeforeNewTry() {
        g2.setColor(Color.DARK_GRAY);
        g2.fillRect(0,0, GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT);
        try {
            BufferedImage level1Image = ImageIO.read(new File("Data/textures/level1.png"));
            g2.drawImage(level1Image, GameFrame.SCREEN_WIDTH/2 - level1Image.getWidth()/2, GameFrame.SCREEN_HEIGHT/2 - level1Image.getHeight()/2, null);
        } catch (Exception e) {}
    }

    public void drawPassLevel1() {
        g2.setColor(Color.DARK_GRAY);
        g2.fillRect(0,0, GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT);
        try {
            BufferedImage level1Image = ImageIO.read(new File("Data/textures/passlevel1.png"));
            g2.drawImage(level1Image, GameFrame.SCREEN_WIDTH/2-level1Image.getWidth()/2, GameFrame.SCREEN_HEIGHT/2-level1Image.getHeight()/2, null);
        } catch (Exception e) {}
    }

    public void loadNewDataBeforeNewTry() {
        player.setPosX(PhysicalMap.PLAYER_X);
        player.setPosY(PhysicalMap.PLAYER_Y);
        camera = new Camera(0, 0, GameFrame.SCREEN_WIDTH, GameFrame.SCREEN_HEIGHT, this);
        gameObjectManager.removeAllEnemiesAndBricks();
        physicalMap.loadEnemyAndBrick();
        player.setAlive(true);
        setNumberOfTries(getNumberOfTries() - 1);
    }


    public void render() {
        if (g2 != null) {
            if (passLevel) {
                drawPassLevel1();
            } else {
                if (numberOfTries > 0) {
                    if (backToCheckPoint) {

                    } else {
                        physicalMap.draw(g2);
                        gameObjectManager.draw(g2);
                        player.draw(g2);
                    }
                }
            }

        }
    }

    public void loadNewLevel() {
        drawPassLevel1();
    }

    public BufferedImage getBufferedImage() {
        return this.bufferedImage;
    }

}
