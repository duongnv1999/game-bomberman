/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject;

import oop.uet.GameObject.ParticularObject.MovableObject.Player;

import java.awt.*;

public class Camera extends GameObject {

    private float widthView;
    private float heigtView;

    public Camera(float posX, float posY, float widthView, float heigtView, GameWorld gameWorld) {
        super(posX, posY, gameWorld);
        this.widthView = widthView;
        this.heigtView = heigtView;
    }

    @Override
    public void update() {
        Player mainCharacter = getGameWorld().player;
        if (mainCharacter.getPosX() < getGameWorld().physicalMap.getWidthOfPhisicalMap()-400) {
            if (mainCharacter.getPosX() >= 400) setPosX(mainCharacter.getPosX() - 400);
        }
    }

    @Override
    public void draw(Graphics2D g2) {}


    public float getWidthView() {
        return widthView;
    }
    public void setWidthView(float widthView) {
        this.widthView = widthView;
    }
    public float getHeigtView() {
        return heigtView;
    }
    public void setHeigtView(float heigtView) {
        this.heigtView = heigtView;
    }
}
