/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.Map;

import oop.uet.GameObject.Camera;
import oop.uet.GameObject.GameObject;
import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Brick;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.BombItem;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.FlameItem;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.Portal;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.SpeedItem;
import oop.uet.GameObject.ParticularObject.MovableObject.Ballon;
import oop.uet.GameObject.ParticularObject.MovableObject.Oneal;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

/**
 * class nay co chuc nang chua map cua game, tao ra cac doi tuong khi load game, ra ve cac loai va cham left, up, right, down
 */

public class PhysicalMap extends GameObject {
    private char[][] phys_Map;
    private int tileSize;
    FrameImage wall, grass;
    public static int PLAYER_X;
    public static int PLAYER_Y;

    public PhysicalMap(float x, float y, GameWorld gameWorld) {
        super(x, y, gameWorld);
        this.tileSize = 40;
        phys_Map = CacheDataLoader.getInstance().getPhysicalMap();
        wall = CacheDataLoader.getInstance().getFrameImage("wall");
        grass = CacheDataLoader.getInstance().getFrameImage("grass");
    }

    public int getTileSize() {
        return this.tileSize;
    }

    //tra ve ki tu thu x, y
    public Character charAt(int x, int y) {
        return phys_Map[x/tileSize][y/tileSize];
    }

    // load enemy, brick, va portal
    public void loadEnemyAndBrick() {
        for (int r = 0; r<phys_Map.length; r++) {
            for (int c = 0; c<phys_Map[0].length; c++) {
                if (phys_Map[r][c] == '*' || phys_Map[r][c] == 'b' ||phys_Map[r][c] == 'f' || phys_Map[r][c] == 'x' || phys_Map[r][c] == 's'){
                    GameObject brick = new Brick(c*tileSize +tileSize/2 , r*tileSize+tileSize/2 , tileSize, tileSize, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(brick);
                }
                if (phys_Map[r][c] == '1') {
                    GameObject ballon = new Ballon(c*tileSize +tileSize/2 , r*tileSize+tileSize/2, 38, 38, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(ballon);
                }
                if (phys_Map[r][c] == '2') {
                    GameObject oneal = new Oneal(c*tileSize +tileSize/2 , r*tileSize+tileSize/2, 38, 38, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(oneal);
                }
                if (phys_Map[r][c] == 'p') {
                    PLAYER_X = c*tileSize + getTileSize()/2;
                    PLAYER_Y = r*tileSize + getTileSize()/2;
                }
                if (phys_Map[r][c] == 'x') {
                    GameObject portal = new Portal(c*tileSize +tileSize/2 , r*tileSize+tileSize/2 , tileSize, tileSize, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(portal);
                }
            }
        }
    }

    //load tat ca cac item trong game
    public void loadItem() {
        for (int r = 0; r<phys_Map.length; r++) {
            for (int c = 0; c<phys_Map[0].length; c++) {
                if (phys_Map[r][c] == 's') {
                    GameObject speedItem = new SpeedItem(c*tileSize +tileSize/2 , r*tileSize+tileSize/2 , tileSize, tileSize, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(speedItem);
                }
                if (phys_Map[r][c] == 'b') {
                    GameObject bombItem = new BombItem(c*tileSize +tileSize/2 , r*tileSize+tileSize/2 , tileSize, tileSize, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(bombItem);
                }
                if (phys_Map[r][c] == 'f') {
                    GameObject flameItem = new FlameItem(c*tileSize +tileSize/2 , r*tileSize+tileSize/2 , tileSize, tileSize, getGameWorld());
                    getGameWorld().gameObjectManager.addObject(flameItem);
                }
            }
        }
    }

    public char[][] getPhys_Map() {
        return phys_Map;
    }

    /**
     * tra ve hinh vuong ma ben trai cua rect va cham voi
     * @param rect hinh chu nhat bao quanh doi tuong
     * @return
     */
    public Rectangle hasCollisionWithLeft(Rectangle rect) {
        int x2 =  (rect.x + rect.width)/tileSize;
        int x1 = 0;
        if (x1 < 0) x1 = 0;
        for (int y = 0; y < phys_Map.length; y++) {
            for (int x = x1; x < x2; x ++) {
                if (phys_Map[y][x] == '#') {
                    Rectangle r = new Rectangle((int)(getPosX() + x*tileSize), (int) ( getPosY() + y*tileSize), tileSize, tileSize);
                    if (rect.intersects(r)) {
                        return r;
                    }
                }
            }
        }
        return null;
    }

    public Rectangle hasCollisionWithRight(Rectangle rect) {
        int x1 = (rect.x+rect.width)/tileSize ;
        int x2 =  phys_Map[0].length;
        if (x1 < 0) x1 = 0;

        for (int y = 0; y < phys_Map.length; y++) {
            for (int x = x1; x < x2; x ++) {
                if (phys_Map[y][x] == '#') {
                    Rectangle r = new Rectangle((int)(getPosX() + x*tileSize), (int) ( getPosY() + y*tileSize), tileSize, tileSize);
                    if (rect.intersects(r)) {
                        return r;
                    }
                }
            }
        }
        return null;
    }

    public Rectangle hasCollisionWithBottom(Rectangle rect) {
        int y1 = rect.y/tileSize;
        int y2 = phys_Map.length;
        if (y1 <0) y1 = 0;
        for (int y = y1; y < y2; y++) {
            for (int x = 0; x < phys_Map[0].length; x ++) {
                if (phys_Map[y][x] == '#') {
                    Rectangle r = new Rectangle((int)(getPosX() + x*tileSize), (int) ( getPosY() + y*tileSize), tileSize, tileSize);
                    if (rect.intersects(r)) {
                        return r;
                    }
                }
            }
        }
        return null;
    }

    public Rectangle hasCollisionWithTop(Rectangle rect) {
        int y1 = 0;
        int y2 = rect.y/tileSize+1;

        if (y2 >= phys_Map.length) y2 = phys_Map.length-1;

        for (int y = y1; y < y2; y++) {
            for (int x = 0; x < phys_Map[0].length; x ++) {
                if (phys_Map[y][x] == '#') {
                    Rectangle r = new Rectangle((int)(getPosX() + x*tileSize), (int) ( getPosY() + y*tileSize), tileSize, tileSize);
                    if (rect.intersects(r)) {
                        return r;
                    }
                }
            }
        }
        return null;
    }

    public int getWidthOfPhisicalMap() {
        return phys_Map[0].length*tileSize;
    }
    public int getHeightOfPhisicalMap() {
        return phys_Map.length*tileSize;
    }

    @Override
    public void draw(Graphics2D g2){
        Camera camera = getGameWorld().camera;
        for (int r = 0; r<phys_Map.length; r++) {
            for (int c = 0; c<phys_Map[0].length; c++) {
                if (phys_Map[r][c] == '#') {
                    wall.draw(g2, (int)(getPosX() + c*tileSize - camera.getPosX()), (int)(getPosY()  + r*tileSize - camera.getPosY()));
                }else {
                    grass.draw(g2,(int)(getPosX() + c*tileSize - camera.getPosX()), (int)(getPosY()  + r*tileSize - camera.getPosY()));
                }
            }
        }
    }

    @Override
    public void update() {}
}
