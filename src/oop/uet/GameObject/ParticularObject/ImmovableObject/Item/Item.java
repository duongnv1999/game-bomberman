/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject.Item;

import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.ImmovableObject;

import java.awt.*;

public abstract class Item  extends ImmovableObject {

    private boolean isEaten;

    public Item(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
    }

    public boolean isEaten() {
        return isEaten;
    }
    public void setEaten(boolean eaten) {
        isEaten = eaten;
    }

    boolean checkCollisionWithBomber() {

        Rectangle speedItemRect = getBoundForCollision();

        if (speedItemRect.intersects(getGameWorld().player.getBoundForCollision())) return true;
        else return false;
    }
}
