/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject.Item;

import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.ImmovableObject;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

public class BombItem extends Item {

    FrameImage powerup_bombs;

    public BombItem(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
        setEaten(false);

        powerup_bombs = CacheDataLoader.getInstance().getFrameImage("powerup_bombs");

        setType("item");
    }

    @Override
    public void update() {
        if(!isEaten()) {
            if (checkCollisionWithBomber()) {
                setEaten(true);
                getGameWorld().player.setCapableOfBomb(getGameWorld().player.getCapableOfBomb()+1);
                System.out.println(getGameWorld().player.getCapableOfBomb());
                getGameWorld().gameObjectManager.removeObject(this);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (!isEaten()) {
            //powerup_bombs.draw(g2, (int) getPosX(), (int) getPosY());
            powerup_bombs.draw(g2,(int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()));

        }
    }
}
