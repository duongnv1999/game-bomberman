/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject.Item;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

public class FlameItem extends Item {
    FrameImage powerup_flames;

    public FlameItem(float posX, float posY,int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);

        setEaten(false);

        powerup_flames = CacheDataLoader.getInstance().getFrameImage("powerup_flames");

        setType("item");
    }


    @Override
    public void update() {
        if(!isEaten()) {
            if (checkCollisionWithBomber()) {
                setEaten(true);
                getGameWorld().player.setSizeOfExplosion(getGameWorld().player.getSizeOfExplosion()+1);
                getGameWorld().gameObjectManager.removeObject(this);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        g2.setColor(Color.red);
        if (!isEaten()) {
            powerup_flames.draw(g2,(int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()));
        }
    }
}
