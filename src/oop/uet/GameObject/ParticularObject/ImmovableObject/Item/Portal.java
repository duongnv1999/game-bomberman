/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject.Item;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

public class Portal extends Item{

    FrameImage portal;

    public Portal(float posX, float posY,int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);

        setEaten(false);

        portal = CacheDataLoader.getInstance().getFrameImage("portal");

        setType("portal");
    }


    @Override
    public void update() {
        if(isEaten()) {
            if (checkCollisionWithBomber()) {
                getGameWorld().setPassLevel(true);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        portal.draw(g2,(int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()));
    }

}
