/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject.Item;

import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.ImmovableObject;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

public class SpeedItem extends Item {

    FrameImage powerup_speed;

    public SpeedItem(float posX, float posY,int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);

        setEaten(false);

        powerup_speed = CacheDataLoader.getInstance().getFrameImage("powerup_speed");

        setType("item");
    }


    @Override
    public void update() {
        if(!isEaten()) {
            if (checkCollisionWithBomber()) {
                setEaten(true);
                getGameWorld().player.setSpeed(getGameWorld().player.getSpeed()+1);
                //System.out.println(getGameWorld().player.getWidth() + " " +getGameWorld().player.getHeight());
                getGameWorld().gameObjectManager.removeObject(this);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.red);
        if (!isEaten()) {
            powerup_speed.draw(g2,(int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()));
        }
    }
}
