/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.Animation;
import oop.uet.effect.CacheDataLoader;
import oop.uet.effect.FrameImage;

import java.awt.*;

public class Brick extends ImmovableObject{

    private boolean isDestroyed;
    FrameImage brick;
    Animation brick_exploded;

    public Brick(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
        setDestroyed(false);
        brick = CacheDataLoader.getInstance().getFrameImage("brick");
        brick_exploded = CacheDataLoader.getInstance().getAnimation("brick_exploded");
        setType("brick");
    }

    public boolean isDestroyed() {
        return isDestroyed;
    }
    public void setDestroyed(boolean destroyed) {
        isDestroyed = destroyed;
    }

    @Override
    public void update() {
        if (!isDestroyed()) {
            //
        } else {
            if (!brick_exploded.isLastFrame()) {
                brick_exploded.update(System.nanoTime());
            } else {
                getGameWorld().gameObjectManager.removeObject(this);
            }
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        if (!isDestroyed) {
            //g2.setColor(Color.cyan);
            //g2.fillRect((int) (getPosX() - getGameWorld().camera.getPosX()), (int) (getPosY() - getGameWorld().camera.getPosY()), getWidth(), getHeight());
            brick.draw(g2, (int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()));
        } else  {
            if (!brick_exploded.isLastFrame()) {
                brick_exploded.draw((int)(getPosX() - getWidth()/2 - getGameWorld().camera.getPosX()), (int) (getPosY() - getHeight()/2 - getGameWorld().camera.getPosY()), g2);
            }
        }
    }
}
