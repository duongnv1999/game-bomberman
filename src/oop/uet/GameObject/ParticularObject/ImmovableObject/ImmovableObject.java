package oop.uet.GameObject.ParticularObject.ImmovableObject;

import oop.uet.GameObject.GameObject;
import oop.uet.GameObject.GameWorld;

import java.awt.*;

public abstract class ImmovableObject extends GameObject {

    private int width;
    private int height;


    public ImmovableObject(float posX, float posY,int width ,int height, GameWorld gameWorld) {
        super(posX, posY, gameWorld);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * lay ra hinh chu nhat la duong vien quanh nhan vat de xu li va cham
     * @return
     */
    public Rectangle getBoundForCollision() {
        Rectangle bound = new Rectangle();
        bound.x = (int)(getPosX() - getWidth()/2);
        bound.y = (int)(getPosY() - getHeight()/2);
        bound.width = (int)getWidth();
        bound.height = (int)getHeight();
        return bound;
    }

    public abstract void draw(Graphics2D g2);
}
