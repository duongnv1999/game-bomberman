/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject;

import oop.uet.GameObject.GameObject;
import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.Map.PhysicalMap;
import oop.uet.GameObject.ParticularObject.GameObjectManager;
import oop.uet.GameObject.ParticularObject.MovableObject.Enemy;
import oop.uet.GameObject.ParticularObject.MovableObject.MovableObject;
import oop.uet.effect.Animation;
import oop.uet.effect.CacheDataLoader;

import java.awt.*;
import java.util.ArrayList;

public class Explosion extends ImmovableObject {

    private int sizeOfExplosion;
    private ArrayList<Rectangle> leftExplosion;
    private ArrayList<Rectangle> rightExplosion;
    private ArrayList<Rectangle> upExplosion;
    private ArrayList<Rectangle> downExplosion;
    private boolean endExplosion;
    private Bomb bomb;
    private long startExplosion;
    Brick brickLeft, brickRight, brickUp, brickDown;

    private Animation bomb_exploded;
    private Animation explosion_vertical;
    private Animation explosion_horizontal;
    private Animation explosion_horizontal_left_last;
    private Animation explosion_horizontal_right_last;
    private Animation explosion_vertical_top_last;
    private Animation explosion_vertical_down_last;

    public Explosion(float posX, float posY, int width, int height, int sizeOfExplosion, GameWorld gameWorld, Bomb bomb, long startExplosion) {
        super(posX, posY, width, height, gameWorld);
        this.sizeOfExplosion = sizeOfExplosion;
        this.bomb = bomb;
        this.startExplosion = startExplosion;

        leftExplosion = new ArrayList<Rectangle>();
        downExplosion = new ArrayList<Rectangle>();
        upExplosion = new ArrayList<Rectangle>();
        rightExplosion = new ArrayList<Rectangle>();

        endExplosion = false;
        setNullBrick();
        verticalExplosion();
        horizontalExplosion();

        bomb_exploded = CacheDataLoader.getInstance().getAnimation("bomb_exploded");
        explosion_vertical = CacheDataLoader.getInstance().getAnimation("explosion_vertical");
        explosion_horizontal = CacheDataLoader.getInstance().getAnimation("explosion_horizontal");
        explosion_horizontal_left_last = CacheDataLoader.getInstance().getAnimation("explosion_horizontal_left_last");
        explosion_horizontal_right_last = CacheDataLoader.getInstance().getAnimation("explosion_horizontal_right_last");
        explosion_vertical_top_last = CacheDataLoader.getInstance().getAnimation("explosion_vertical_top_last");
        explosion_vertical_down_last = CacheDataLoader.getInstance().getAnimation("explosion_vertical_down_last");

        setType("explosion");
    }

    public void setNullBrick(){
        this.brickLeft = null;
        this.brickDown = null;
        this.brickRight = null;
        this.brickLeft = null;
    }

    /**
     * kiem tra xem verticalExplosion co va cham voi wall, brick, player, enemy hay khong
     */
    public void verticalExplosion() {
        Rectangle rect = bomb.getBoundForCollision();
        PhysicalMap map = getGameWorld().physicalMap;
        int tileSize = map.getTileSize();

        for (int up = 1; up <= sizeOfExplosion; up++) {
            Rectangle verticalExplUp = new Rectangle((int)(rect.x),(int) (rect.y - up*tileSize ), getWidth(), getHeight());

            //neu vi tri hien tai la grass, thi explosion co the no
            if (map.charAt(rect.y - up*tileSize, rect.x) == ' ' || getGameWorld().gameObjectManager.isCollisionWithEnemyObject(verticalExplUp)) {
                upExplosion.add(verticalExplUp);
            } else if (map.charAt(rect.y - up*tileSize, rect.x) == '#') {
                break;
            } else {

                //neu va cham vao gach, thi them vao 1 explosion va break ra khoi vong lap
                upExplosion.add(verticalExplUp);
                Rectangle temp = new Rectangle(verticalExplUp.x, verticalExplUp.y, tileSize, tileSize);
                brickUp =(Brick)getGameWorld().gameObjectManager.getBrick(temp);
                if (brickUp != null) {
                    break;
                }
            }
        }

        for (int down = 1; down <= sizeOfExplosion; down++) {
            Rectangle verticalExplDown = new Rectangle((int)(rect.x),(int) (rect.y + down*tileSize ), getWidth(), getHeight());
            if (map.charAt(rect.y + down*tileSize, rect.x) == ' ' || getGameWorld().gameObjectManager.isCollisionWithEnemyObject(verticalExplDown)) {
                downExplosion.add(verticalExplDown);
            }  else if (map.charAt(rect.y + down*tileSize, rect.x) == '#') {
                break;
            } else {
                downExplosion.add(verticalExplDown);
                Rectangle temp = new Rectangle(verticalExplDown.x, verticalExplDown.y, tileSize, tileSize);
                brickDown =(Brick)getGameWorld().gameObjectManager.getBrick(temp);
                if (brickDown != null) {
                    break;
                }
            }
        }
    }


    public void horizontalExplosion() {
        Rectangle rect = bomb.getBoundForCollision();
        PhysicalMap map = getGameWorld().physicalMap;
        int tileSize = getGameWorld().physicalMap.getTileSize();

        for (int left = 1; left <= sizeOfExplosion; left++) {
            Rectangle horizontalExplLeft = new Rectangle((int) (rect.x -  left* tileSize), (int) (rect.y), getWidth(), getHeight());
            if (map.charAt(rect.y, rect.x -  left* tileSize) == ' ' || getGameWorld().gameObjectManager.isCollisionWithEnemyObject(horizontalExplLeft)) {
                leftExplosion.add(horizontalExplLeft);
            }  else if (map.charAt(rect.y, rect.x -  left* tileSize) == '#') {
                break;
            } else {
                leftExplosion.add(horizontalExplLeft);
                Rectangle temp = new Rectangle(horizontalExplLeft.x, horizontalExplLeft.y, tileSize, tileSize);
                brickLeft =(Brick)getGameWorld().gameObjectManager.getBrick(temp);
                if (brickLeft != null) {
                    break;
                }
            }
        }

        for (int right = 1; right <= sizeOfExplosion; right++) {
            Rectangle horizontalExplRight = new Rectangle((int) (rect.x + right * tileSize), (int) (rect.y), getWidth(), getHeight());
            if (map.charAt(rect.y, rect.x +  right* tileSize) == ' ' || getGameWorld().gameObjectManager.isCollisionWithEnemyObject(horizontalExplRight)) {
                rightExplosion.add(horizontalExplRight);
            }  else if (map.charAt(rect.y, rect.x +  right* tileSize) == '#') {
                break;
            } else {
                rightExplosion.add(horizontalExplRight);
                Rectangle temp = new Rectangle(horizontalExplRight.x, horizontalExplRight.y, tileSize, tileSize);
                brickRight =(Brick)getGameWorld().gameObjectManager.getBrick(temp);
                if (brickRight != null) {
                    break;
                }
            }
        }

    }


    public void setStartExplosion(long startExplosion) {
        this.startExplosion = startExplosion;
    }

    public int getSizeOfExplosion() {
        return sizeOfExplosion;
    }

    public void setSizeOfExplosion(int sizeOfExplosion) {
        this.sizeOfExplosion = sizeOfExplosion;
    }

    public void checkCollisionWithEnemy() {
        for (int left = 0; left < leftExplosion.size(); left++) {

            GameObject enemy = getGameWorld().gameObjectManager.isCollisionWithEnemy(leftExplosion.get(left));
            if (enemy!= null) {
                //neuva cham voi enemy, thi set enemy do die
                ((MovableObject)enemy).setAlive(false);
            }
            // neu va cham voi player thi player se duoc set la not alive
            if (getGameWorld().player.getBoundForCollision().intersects(leftExplosion.get(left))) {
                getGameWorld().player.setAlive(false);
            }
        }
        for (int right = 0; right < rightExplosion.size(); right++) {
            GameObject enemy = getGameWorld().gameObjectManager.isCollisionWithEnemy(rightExplosion.get(right));
            if (enemy!= null) {
                ((MovableObject)enemy).setAlive(false);
            }
            if (getGameWorld().player.getBoundForCollision().intersects(rightExplosion.get(right))) {
                getGameWorld().player.setAlive(false);
            }
        }
        for (int up = 0; up < upExplosion.size(); up++) {
            GameObject enemy = getGameWorld().gameObjectManager.isCollisionWithEnemy(upExplosion.get(up));
            if (enemy!= null) {
                ((MovableObject)enemy).setAlive(false);
            }
            if (getGameWorld().player.getBoundForCollision().intersects(upExplosion.get(up))) {
                getGameWorld().player.setAlive(false);
            }
        }
        for (int down = 0; down < downExplosion.size(); down++) {
            GameObject enemy = getGameWorld().gameObjectManager.isCollisionWithEnemy(downExplosion.get(down));
            if (enemy!= null) {
                ((MovableObject)enemy).setAlive(false);
            }
            if (getGameWorld().player.getBoundForCollision().intersects(downExplosion.get(down))) {
                getGameWorld().player.setAlive(false);
            }
        }

        //tai vi tri bomb neu va cham voi player thi player se duoc set la not alive
        if (bomb.getBoundForCollision().intersects(getGameWorld().player.getBoundForCollision())) {
            getGameWorld().player.setAlive(false);
        }
    }

    public void updateAllAnimation(long time) {
        bomb_exploded.update(time);
        explosion_vertical.update(time);
        explosion_horizontal.update(time);
        explosion_vertical_down_last.update(time);
        explosion_vertical_top_last.update(time);
        explosion_horizontal_right_last.update(time);
        explosion_horizontal_left_last.update(time);
    }

    @Override
    public void update() {
        long deltaTime = System.nanoTime() - startExplosion;
        if (bomb.getExploded()) {
            if (deltaTime < 0.5 * 1000000000) {
                checkCollisionWithEnemy();
            } else {
                endExplosion = true;
                if (brickLeft != null) brickLeft.setDestroyed(true);
                if (brickUp != null) brickUp.setDestroyed(true);
                if (brickRight != null) brickRight.setDestroyed(true);
                if (brickDown != null) brickDown.setDestroyed(true);
                getGameWorld().player.setCapableOfBomb(getGameWorld().player.getCapableOfBomb()+1);
                getGameWorld().gameObjectManager.removeObject(this.bomb);
            }
        }
        updateAllAnimation(System.nanoTime());
    }

    @Override
    public void draw(Graphics2D g2) {
        g2.setColor(Color.GREEN);

        if (!endExplosion) {

            bomb_exploded.draw((int)(bomb.getPosX() - getWidth()/2 -getGameWorld().camera.getPosX()), (int)(bomb.getPosY() - getHeight()/2 -getGameWorld().camera.getPosY()), g2 );

            for (int left = leftExplosion.size()-1; left >= 0 ; left--) {
                if (left == leftExplosion.size()-1) {
                    explosion_horizontal_left_last.draw((int)(leftExplosion.get(left).getX() -getGameWorld().camera.getPosX()), (int)(leftExplosion.get(left).getY() -getGameWorld().camera.getPosY()), g2);
                } else {
                    explosion_horizontal.draw((int)(leftExplosion.get(left).getX() -getGameWorld().camera.getPosX()), (int)(leftExplosion.get(left).getY() -getGameWorld().camera.getPosY()), g2);
                }
            }
            for (int right =  rightExplosion.size()-1; right >= 0; right--) {
                if (right ==  rightExplosion.size()-1 ) {
                    explosion_horizontal_right_last.draw((int)(rightExplosion.get(right).getX() -getGameWorld().camera.getPosX()), (int)(rightExplosion.get(right).getY() -getGameWorld().camera.getPosY()), g2);
                } else {
                    explosion_horizontal.draw((int)(rightExplosion.get(right).getX() -getGameWorld().camera.getPosX()), (int)(rightExplosion.get(right).getY() -getGameWorld().camera.getPosY()), g2);
                }
            }
            for (int up = upExplosion.size()-1; up >= 0; up--) {
                if (up == upExplosion.size()-1) {
                    explosion_vertical_top_last.draw((int)(upExplosion.get(up).getX() -getGameWorld().camera.getPosX()), (int)(upExplosion.get(up).getY() -getGameWorld().camera.getPosY()), g2);
                } else  {
                    explosion_vertical.draw((int)(upExplosion.get(up).getX() -getGameWorld().camera.getPosX()), (int)(upExplosion.get(up).getY() -getGameWorld().camera.getPosY()), g2);
                }
            }
            for (int down = downExplosion.size()-1; down >=0 ; down--) {
                if (down == downExplosion.size()-1) {
                    explosion_vertical_down_last.draw((int)(downExplosion.get(down).getX() -getGameWorld().camera.getPosX()), (int)(downExplosion.get(down).getY() -getGameWorld().camera.getPosY()), g2);
                } else {
                    explosion_vertical.draw((int)(downExplosion.get(down).getX() -getGameWorld().camera.getPosX()), (int)(downExplosion.get(down).getY() -getGameWorld().camera.getPosY()), g2);
                }
            }
        }
    }
}