
/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.ImmovableObject;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.Animation;
import oop.uet.effect.CacheDataLoader;

import java.awt.*;

public class Bomb extends ImmovableObject{

    boolean isExploded;
    long startTimeExplode;
    int explosionTime;
    boolean allowMoveThru;
    Explosion explosion;

    Animation bomb;

    public Bomb(float posX, float posY, int width, int height, GameWorld gameWorld, int explosionTime,int sizeOfExplosion, long startTimeExplode) {
        super(posX, posY, width, height, gameWorld);
        this.explosionTime = explosionTime;
        this.startTimeExplode = startTimeExplode;
        setType("bomb");
        getGameWorld().gameObjectManager.addObject(this);
        explosion = new Explosion(posX, posY, width, height, sizeOfExplosion, getGameWorld(), this, System.nanoTime()+explosionTime*1000000000);
        bomb = CacheDataLoader.getInstance().getAnimation("bomb");
    }


    public boolean getExploded() {
        return isExploded;
    }
    public void setExploded(boolean isExploded) {
        this.isExploded = isExploded;
    }

    /**
     * startTimeToExplode la thoi gian bat dau ke tu khi dat bomb
     */
    public long getStartTimeExplode() {
        return startTimeExplode;
    }
    public void setStartTimeExplode(long timeToExplode) {
        this.startTimeExplode = timeToExplode;
    }

    public boolean isAllowMoveThru() {
        return allowMoveThru;
    }
    public void setAllowMoveThru(boolean allowMoveThru) {
        this.allowMoveThru = allowMoveThru;
    }

    @Override
    public void update() {
        long deltaTime = System.nanoTime() - startTimeExplode;
        if (deltaTime < explosionTime*1000000000) {
            bomb.update(System.nanoTime());
        } else {
            explosion.update();
            setExploded(true);
            //getGameWorld().player.setCapableOfBomb(getGameWorld().player.getCapableOfBomb()+1);
            //play animation
            //getGameWorld().gameObjectManager.removeObject(this);
        }
    }

    @Override
    public void draw(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        g2.setColor(Color.green);
        if (!isExploded) {
            bomb.draw((int)(rect.x - getGameWorld().camera.getPosX()),(int) (rect.y - getGameWorld().camera.getPosY()), g2);
        } else  {
            explosion.draw(g2);
        }
    }




}
