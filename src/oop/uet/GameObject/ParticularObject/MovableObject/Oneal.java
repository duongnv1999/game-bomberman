package oop.uet.GameObject.ParticularObject.MovableObject;

import oop.uet.GameObject.GameWorld;
import oop.uet.effect.Animation;
import oop.uet.effect.CacheDataLoader;

import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class Oneal extends Enemy{

    private boolean dir_up;
    private boolean dir_down;
    private boolean dir_left;
    private boolean dir_right;

    private long startTime;
    private long timeToMove;
    private long currentTime;

    private Animation oneal_left;
    private Animation oneal_right;
    private Animation oneal_dead;

    Animation ballon;


    //constructor
    public Oneal(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
        setSpeed(1);
        setAlive(true);
        setType("enemy");

        oneal_left = CacheDataLoader.getInstance().getAnimation("oneal_left");
        oneal_right = CacheDataLoader.getInstance().getAnimation("oneal_right");
        oneal_dead = CacheDataLoader.getInstance().getAnimation("oneal_dead");
    }

    /**
     * getter and setter
     */
    public void setDir_up(boolean dir_up) {
        this.dir_up = dir_up;
    }
    public void setDir_down(boolean dir_down) {
        this.dir_down = dir_down;
    }
    public void setDir_left(boolean dir_left) {
        this.dir_left = dir_left;
    }
    public void setDir_right(boolean dir_right) {
        this.dir_right = dir_right;
    }

    void randomDirection() {
        int randomMove = ThreadLocalRandom.current().nextInt(4);
        switch (randomMove) {
            case 0 :
            {
                setDir_up(true);
                break;
            }
            case 1 :
            {
                setDir_right(true);
                break;
            }
            case 2 :
            {
                setDir_down(true);
                break;
            }
            case 3 :
            {
                setDir_left(true);
                break;
            }
        }
    }

    private void setAllFalse() {
        setDir_left(false);
        setDir_down(false);
        setDir_right(false);
        setDir_up(false);
    }

    /**
     * tra ve thoi gian random giua cac lan di chuyen cua ballom
     * @return
     */
    public double RandomTimeToMove() {
        return ThreadLocalRandom.current().nextDouble(1,3);
    }

    /**
     * kiem tra xem co va cham voi nhan vat hay khong
     * @return
     */
    public boolean checkCollisionWithPlayer(){
        Rectangle playerRect = getGameWorld().player.getBoundForCollision();
        Rectangle ballonRect = getBoundForCollision();
        if (playerRect.intersects(ballonRect)) {
            return true;
        }
        return false;
    }

    public void updateAllAnimation(long time) {
        oneal_left.update(time);
        oneal_right.update(time);
    }

    public void update() {
        if (isAlive()) {
            currentTime = System.nanoTime();
            long deltaTime = currentTime - startTime;
            timeToMove = (long) RandomTimeToMove() * (long) 1000000000;
            if (deltaTime < timeToMove) {
                if (dir_right) moveToRight();
                if (dir_left) moveToLeft();
                if (dir_down) moveDown();
                if (dir_up) moveUp();
            } else {
                setAllFalse();
                randomDirection();
                startTime = System.nanoTime();
            }
            if (checkCollisionWithPlayer()) {
                getGameWorld().player.setAlive(false);
            }
            updateAllAnimation(System.nanoTime());
            this.setSpeed(ThreadLocalRandom.current().nextInt(1, 4));
        }
    }


    public void draw(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        if (isAlive()) {
            if (dir_up || dir_left) oneal_left.draw((int)(rect.x - getGameWorld().camera.getPosX()),(int) (rect.y - getGameWorld().camera.getPosY()),g2);
            if (dir_down || dir_right) oneal_right.draw((int)(rect.x - getGameWorld().camera.getPosX()),(int) (rect.y - getGameWorld().camera.getPosY()),g2);
        } else {
            oneal_dead.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
            getGameWorld().gameObjectManager.removeObject(this);
        }
    }

    public void drawBoundForCollisionWithMap(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        g2.setColor(Color.BLACK);
        g2.fillRect((int)(rect.x - getGameWorld().camera.getPosX()),(int) (rect.y - getGameWorld().camera.getPosY()), getWidth(), getHeight());
    }

}
