/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */
package oop.uet.GameObject.ParticularObject.MovableObject;

import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Bomb;
import oop.uet.GameObject.ParticularObject.MovableObject.MovableObject;
import oop.uet.effect.Animation;
import oop.uet.effect.CacheDataLoader;

import java.awt.*;

/**
 * class tuong trung cho nguoi choi, xu li cac cong viec nhu dat bomb, di chuyen nhan vat...
 */
public class Player extends MovableObject {

    Animation player_up, player_down, player_left, player_right, player_dead;
    long timeToNextBomb;
    long currentBombTime;
    int capableOfBomb;
    int sizeOfExplosion = 1;
    int timeToSetNextBomb;
    Bomb bomb;

    private boolean KEY_UP;
    private boolean KEY_DOWN;
    private boolean KEY_LEFT;
    private boolean KEY_RIGHT;
    private boolean KEY_SPACE;

    int lastFrameImage;

    //constructor
    public Player(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
        setAlive(true);
        setCapableOfBomb(1);
        timeToNextBomb = 0;

        player_left = CacheDataLoader.getInstance().getAnimation("player_left");
        player_down = CacheDataLoader.getInstance().getAnimation("player_down");
        player_right = CacheDataLoader.getInstance().getAnimation("player_right");
        player_up = CacheDataLoader.getInstance().getAnimation("player_up");
        player_dead = CacheDataLoader.getInstance().getAnimation("player_dead");

        setType("player");

    }

    /**
     * getter and setter
     */
    public int getCapableOfBomb() {
        return capableOfBomb;
    }
    public void setCapableOfBomb(int capableOfBomb) {
        this.capableOfBomb = capableOfBomb;
    }

    public void setKEY_UP(boolean KEY_UP) {
        this.KEY_UP = KEY_UP;
    }

    public void setKEY_DOWN(boolean KEY_DOWN) {
        this.KEY_DOWN = KEY_DOWN;
    }

    public void setKEY_LEFT(boolean KEY_LEFT) {
        this.KEY_LEFT = KEY_LEFT;
    }

    public void setKEY_RIGHT(boolean KEY_RIGHT) {
        this.KEY_RIGHT = KEY_RIGHT;
    }

    public void setKEY_SPACE(boolean KEY_SPACE) {
        this.KEY_SPACE = KEY_SPACE;
    }

    public boolean isKEY_SPACE() {
        return KEY_SPACE;
    }

    public int getSizeOfExplosion() {
        return sizeOfExplosion;
    }

    public void setSizeOfExplosion(int sizeOfExplosion) {
        this.sizeOfExplosion = sizeOfExplosion;
    }

    /**
     * tinh gia tri X de dat bomb
     *
     * @return tra ve gia tri x la vi tri co the dat bomb
     */
    public int calculateXPosToSetBomb() {
        int x1 = (int) getPosX();
        int x2 = (int) getPosX();
        while (x1 > 0) {
            if (x1 % getGameWorld().physicalMap.getTileSize() == 0)
                return x1 + getGameWorld().physicalMap.getTileSize() / 2;
            if (x2 % getGameWorld().physicalMap.getTileSize() == 0)
                return x2 - getGameWorld().physicalMap.getTileSize() / 2;
            x1--;
            x2++;
        }
        return 0;
    }

    /**
     * tinh gia tri Y de dat bomb
     *
     * @return tra ve gia tri Y la vi tri co the dat bomb
     */
    public int calculateYPosToSetBomb() {
        int y1 = (int) getPosY();
        int y2 = (int) getPosY();
        while (y1 > 0) {
            if (y1 % getGameWorld().physicalMap.getTileSize() == 0)
                return y1 + getGameWorld().physicalMap.getTileSize() / 2;
            if (y2 % getGameWorld().physicalMap.getTileSize() == 0)
                return y2 - getGameWorld().physicalMap.getTileSize() / 2;
            y1--;
            y2++;
        }
        return 0;
    }


    public void update() {
        if (isAlive()) {
            if (KEY_DOWN) {
                moveDown();
                player_down.update(System.nanoTime());
            }
            if (KEY_UP) {
                moveUp();
                player_up.update(System.nanoTime());
            }
            if (KEY_RIGHT) {
                moveToRight();
                player_right.update(System.nanoTime());
            }
            if (KEY_LEFT) {
                moveToLeft();
                player_left.update(System.nanoTime());
            }
            if (KEY_SPACE) {
                if (getCapableOfBomb() > 0 && System.nanoTime() - timeToNextBomb >= 200000000) {
                    bomb = new Bomb(calculateXPosToSetBomb(), calculateYPosToSetBomb(), 40, 40, getGameWorld(), 2, sizeOfExplosion, System.nanoTime());
                    setCanMoveThroughBomb(true);
                    capableOfBomb -= 1;
                    timeToNextBomb = System.nanoTime();
                }
            }
            if (bomb != null && !bomb.getBoundForCollision().intersects(this.getBoundForCollision())) {
                setCanMoveThroughBomb(false);
            }
        } else {
            //if (player_dead.isLastFrame()) {
                player_dead.update(System.nanoTime());
            //}
        }
    }


    public void draw(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        //drawBoundForCollisionWithMap(g2);
        if (isAlive()) {
            if (KEY_DOWN) {
                player_down.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
                lastFrameImage = 2;
            } else if (KEY_UP) {
                player_up.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
                lastFrameImage = 0;
            } else if (KEY_RIGHT) {
                player_right.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
                lastFrameImage = 1;
            } else if (KEY_LEFT) {
                player_left.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
                lastFrameImage = 3;
            } else {
                switch (lastFrameImage) {
                    case 0: {
                        player_up.getFrameImageAt(0).draw(g2 ,(int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()));
                        break;
                    }
                    case 1: {
                        player_right.getFrameImageAt(0).draw(g2 ,(int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()));
                        break;
                    }
                    case 2: {
                        player_down.getFrameImageAt(0).draw(g2 ,(int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()));
                        break;
                    }
                    case 3: {
                        player_left.getFrameImageAt(0).draw(g2 ,(int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()));
                        break;
                    }
                }
            }
        } else {

            //neu nhan vat chet, khi nao animation chay toi frame cuoi cung thi tra nhan vat ve trang thai ban dau voi buff giu nguyen
            if (!player_dead.isLastFrame()) {
                player_dead.draw((int) (rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()), g2);
            } else {
                player_dead.nextFrame();
                getGameWorld().setBackToCheckPoint(true, System.nanoTime());
            }
        }
    }

    public void drawBoundForCollisionWithMap(Graphics2D g2) {
        Rectangle rect = getBoundForCollision();
        //g2.fillRect((int)(rect.x - getGameWorld().camera.getPosX()), (int) (rect.y - getGameWorld().camera.getPosY()),(int)rect.getWidth(), (int)rect.getHeight());
    }

}
