package oop.uet.GameObject.ParticularObject.MovableObject;

import oop.uet.GameObject.GameWorld;

public abstract class Enemy extends MovableObject {

    public Enemy(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, width, height, gameWorld);
    }

}
