/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject.MovableObject;

import oop.uet.GameObject.GameObject;
import oop.uet.GameObject.GameWorld;

import java.awt.*;

/**
 * class tuong trung cho cac doi tuong co the di chuyen duoc trong game
 */
public abstract class MovableObject extends GameObject {
    private boolean isAlive;
    private int speed = 1;
    private int width;
    private int height;
    private boolean canMoveThroughBomb;

    public MovableObject(float posX, float posY, int width, int height, GameWorld gameWorld) {
        super(posX, posY, gameWorld);
        this.width = width;
        this.height = height;
    }

    public boolean isAlive() {
        return isAlive;
    }
    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public boolean CanMoveThroughBomb() {
        return canMoveThroughBomb;
    }
    public void setCanMoveThroughBomb(boolean canMoveThroughBomb) {
        this.canMoveThroughBomb = canMoveThroughBomb;
    }

    // kiem tra xem nhan vat co the di chuyen duoc qua bomb hay khong
    boolean checkMoveThroughBomb(Rectangle rect) {
        if (!canMoveThroughBomb && getGameWorld().gameObjectManager.isCollisionWithBomb(rect)) return true;
        return false;
    }

    public void moveToLeft() {
        // futureRect la hinh chu nhat ma trong lan di chuyen tiep theo, doi tuong se cham phai
        Rectangle futureRect = getBoundForCollision();
        futureRect.x -= speed;

        //kiem tra xem neu co va cham voi hinh futureRect
        //hoac va cham voi brick
        //hoac va cham voi portal
        Rectangle rectLane = getGameWorld().physicalMap.hasCollisionWithLeft(futureRect);
        Rectangle collisionWithBrick = getGameWorld().gameObjectManager.isCollisionWithBrick(futureRect);
        boolean collisionWithPortal = getGameWorld().gameObjectManager.isCollisionWithPortal(futureRect);
        if (rectLane != null || collisionWithBrick != null || checkMoveThroughBomb(futureRect) || collisionWithPortal) {
        } else  {
            setPosX(getPosX() - speed);
        }
    }
    public void moveToRight() {
        Rectangle futureRect = getBoundForCollision();
        futureRect.x += speed;
        Rectangle rectLane = getGameWorld().physicalMap.hasCollisionWithRight(futureRect);
        Rectangle collisionWithBrick = getGameWorld().gameObjectManager.isCollisionWithBrick(futureRect);
        boolean collisionWithPortal = getGameWorld().gameObjectManager.isCollisionWithPortal(futureRect);
        if (rectLane != null || collisionWithBrick != null || checkMoveThroughBomb(futureRect) || collisionWithPortal) {
        } else  {
            setPosX(getPosX() + speed);
        }
    }
    public void moveUp() {
        Rectangle futureRect = getBoundForCollision();
        futureRect.y -= speed;
        Rectangle rectLane = getGameWorld().physicalMap.hasCollisionWithTop(futureRect);
        Rectangle collisionWithBrick = getGameWorld().gameObjectManager.isCollisionWithBrick(futureRect);
        boolean collisionWithPortal = getGameWorld().gameObjectManager.isCollisionWithPortal(futureRect);
        if (rectLane != null || collisionWithBrick != null || checkMoveThroughBomb(futureRect) || collisionWithPortal) {
        } else  {
            setPosY(getPosY() - speed);
        }
    }
    public void moveDown() {
        Rectangle futureRect = getBoundForCollision();
        futureRect.y += speed;
        Rectangle rectLane = getGameWorld().physicalMap.hasCollisionWithBottom(futureRect);
        Rectangle collisionWithBrick = getGameWorld().gameObjectManager.isCollisionWithBrick(futureRect);
        boolean collisionWithPortal = getGameWorld().gameObjectManager.isCollisionWithPortal(futureRect);
        if (rectLane != null || collisionWithBrick != null || checkMoveThroughBomb(futureRect) || collisionWithPortal) {
        } else  {
            setPosY(getPosY() + speed);
        }
    }

    /**
     * lay ra hinh chu nhat la duong vien quanh nhan vat de xu li va cham
     * @return
     */
    public Rectangle getBoundForCollision() {
        Rectangle bound = new Rectangle();
        bound.x = (int)(getPosX() - getWidth()/2);
        bound.y = (int)(getPosY() - getHeight()/2);
        bound.width = (int)getWidth();
        bound.height = (int)getHeight();
        return bound;
    }


    public abstract void draw(Graphics2D g2);
}
