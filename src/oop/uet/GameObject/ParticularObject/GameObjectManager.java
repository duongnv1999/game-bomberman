/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.GameObject.ParticularObject;

/**
 * class chua tat cac cac doi tuong huu hinh, xuat hien trong game. va xu li va cham giua chung voi nhung doi tuong khac
 */

import oop.uet.GameObject.GameObject;
import oop.uet.GameObject.GameWorld;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Bomb;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Brick;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.Item;
import oop.uet.GameObject.ParticularObject.ImmovableObject.Item.Portal;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GameObjectManager {
    List<GameObject> objects;
    List<GameObject> enemies;
    List<GameObject> bricks;
    List<GameObject> bombs;
    List<GameObject> items;
    List<GameObject> portal;
    private GameWorld gameWorld;

    public GameObjectManager(GameWorld gameWorld) {
        objects = new ArrayList<GameObject>();
        enemies = new  ArrayList<GameObject>();
        bricks = new ArrayList<GameObject>();
        bombs = new ArrayList<GameObject>();
        items = new ArrayList<GameObject>();
        portal = new ArrayList<GameObject>();
        this.gameWorld = gameWorld;
    }

    public void addObject(GameObject object) {
        if (object.getType().equals("enemy")) {
            enemies.add(object);
        } else if (object.getType().equals("brick")) {
            bricks.add(object);
        } else if (object.getType().equals("bomb")) {
            bombs.add(object);
        } else if (object.getType().equals("item")) {
            items.add(object);
        } else if (object.getType().equals("portal")) {
            portal.add(object);
        } else {
            objects.add(object);
        }
    }

    /**
     * xoa 1 object trong so cac object trong manager
     * @param object
     */
    public void removeObject(GameObject object) {
        for (int i=0; i<objects.size(); i++) {
            if (objects.get(i) == object) {
                objects.remove(i);
            }
        }
        for (int j=0; j<bricks.size(); j++) {
            if (bricks.get(j) == object) {
                bricks.remove(j);
            }
        }
        for (int k=0; k<bombs.size(); k++) {
            if (bombs.get(k) == object) {
                bombs.remove(k);
            }
        }
        for (int e=0; e<enemies.size(); e++) {
            if (enemies.get(e) == object) {
                enemies.remove(e);
            }
        }
    }

    public void updateAllObject() {
        for (int i=0; i<objects.size(); i++) {
            objects.get(i).update();
        }
        for (int k=0; k<bombs.size(); k++) {
            bombs.get(k).update();
        }
        for (int t=0; t<items.size(); t++) {
            items.get(t).update();
        }
        for (int p=0; p<portal.size(); p++) {
            portal.get(p).update();
        }
        for (int j=0; j<bricks.size(); j++) {
            bricks.get(j).update();
        }
        for (int e=0; e<enemies.size(); e++) {
            enemies.get(e).update();
        }

        //khi da het enemy thi portal co the di qua duoc, va ket thuc man choi
        if (enemies.size() == 0) {
            if (portal.size() != 0) {
                for (GameObject por : portal) {
                    ((Portal)por).setEaten(true);
                }
            }
        }
    }

    public void draw(Graphics2D g2) {

        for (int i=0; i<objects.size(); i++) {
            objects.get(i).draw(g2);
        }
        for (int t=0; t<items.size(); t++) {
            items.get(t).draw(g2);
        }
        for (int p=0; p<portal.size(); p++) {
            portal.get(p).draw(g2);
        }
        for (int j=0; j<bricks.size(); j++) {
            bricks.get(j).draw(g2);
        }
        for (int k=0; k<bombs.size(); k++) {
            bombs.get(k).draw(g2);
        }
        for (int e=0; e<enemies.size(); e++) {
            enemies.get(e).draw(g2);
        }
    }

    /**
     * tra ve brick ma Rect truyen vao va cham voi, neu khong tra ve null
     * @param rect
     * @return
     */
    public GameObject getBrick(Rectangle rect) {
        for (GameObject brick : bricks) {
            if (rect.intersects(brick.getBoundForCollision())) {
                return brick;
            }
        }
        return null;
    }

    /**
     * tra ve true neu Rect truyen vao va cham voi bat ki enemy nao, neu khong tra ve false
     * @param rect
     * @return
     */
    public boolean isCollisionWithEnemyObject(Rectangle rect) {
        if (enemies.size() == 0) return false;
        else {
            for (GameObject object : objects) {
                if (object.getType().equals("enemy")) {
                    if (object.getBoundForCollision().intersects(rect)) return true;
                }
            }
        }
        return false;
    }

    public Rectangle isCollisionWithBrick(Rectangle rect) {
        if (bricks.size() == 0) return null;
        else {
            for (GameObject brick : bricks) {
                if (rect.intersects(brick.getBoundForCollision())) {
                    return brick.getBoundForCollision();
                }
            }
        }
        return null;
    }

    /**
     * tra ve true neu rect truyen vao va cham voi bat kia qua bomb nao
     * @param rect
     * @return
     */
    public boolean isCollisionWithBomb(Rectangle rect) {
        if (bombs.size() == 0) return false;
        else {
            for (GameObject bomb : bombs) {
                if (rect.intersects(bomb.getBoundForCollision())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * tra ve enemy neu rect truyen vao va cham voi enemy do, khong thi tra ve null
     * @param rect
     * @return
     */
    public GameObject isCollisionWithEnemy(Rectangle rect) {
        if (enemies.size() == 0) return null;
        else  {
            for (GameObject enemy : enemies) {
                if (rect.intersects(enemy.getBoundForCollision())) {
                    return enemy;
                }
            }
        }
        return null;
    }

    /**
     * tra ve true neu portal co the di qua duoc, va va cham voi rect truyen vao
     * @param rect
     * @return
     */
    public boolean isCollisionWithPortal(Rectangle rect) {
        if (portal.size() == 0) return false;
        else  {
            for (GameObject por : portal) {
                if (rect.intersects(por.getBoundForCollision()) && !((Portal)por).isEaten()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * xoa sach enemy va brick de chuan bi cho man choi tiep theo
     */
    public void removeAllEnemiesAndBricks() {
        enemies.clear();
        bricks.clear();
        objects.clear();
    }
}
