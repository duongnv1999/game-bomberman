/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.effect;

import java.awt.*;
import java.awt.image.BufferedImage;

public class FrameImage {
    private String name;
    private BufferedImage Image;

    public FrameImage (String name, BufferedImage Image) {
        this.name = name;
        this.Image = Image;
    }
    public FrameImage(FrameImage frameImage) {
        this.Image = new BufferedImage(frameImage.getImageWidth(), frameImage.getImageHeight(), frameImage.getImage().getType());
        Graphics g = Image.getGraphics();
        g.drawImage(frameImage.getImage(), 0,0, null);
    }

    public FrameImage() {
        this.name = null;
        this.Image = null;
    }

    public void draw(Graphics2D g2, int x, int y) {
        //g2.drawImage(Image, x-Image.getWidth()/2, y-Image.getHeight()/2, null);
        g2.drawImage(Image, x, y, null);
    }

    //
    public int getImageWidth() {
        return Image.getWidth();
    }
    public int getImageHeight() {
        return Image.getHeight();
    }

    //getter and setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public BufferedImage getImage() {
        return Image;
    }
    public void setImage(BufferedImage image) {
        Image = image;
    }
}
