/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.effect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.util.ArrayList;

/**
 * xu li cac animation trong game
 */

public class Animation {
    private String name;

    private boolean isRepeated;

    private ArrayList<FrameImage> frameImages;
    private int currentFrame;

    private ArrayList<Boolean> ignoreFrame;

    private ArrayList<Double> delayTime;

    private long beginTime;

    private boolean drawRectFrame;

    //constructor
    public Animation() {
        isRepeated = true;
        beginTime = 0;
        drawRectFrame = false;
        frameImages = new ArrayList<FrameImage>();
        ignoreFrame = new ArrayList<Boolean>();
        delayTime = new ArrayList<Double>();
    }

    //coppy constructor
    public Animation(Animation animation) {
        isRepeated = animation.getRepeated();
        beginTime = animation.getBeginTime();
        drawRectFrame = animation.getDrawRectFrame();

        frameImages = new ArrayList<FrameImage>();
        for (FrameImage o : animation.frameImages) {
            frameImages.add(o);
        }

        ignoreFrame = new ArrayList<Boolean>();
        for (Boolean b : animation.ignoreFrame) {
            ignoreFrame.add(b);
        }

        delayTime = new ArrayList<Double>();
        for (Double d : animation.delayTime) {
            delayTime.add(d);
        }
    }


    // getter and setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean getRepeated() {
        return isRepeated;
    }
    public void setRepeated(boolean repeated) {
        isRepeated = repeated;
    }
    public ArrayList<FrameImage> getFrameImage() {
        return frameImages;
    }
    public void setFrameImage(ArrayList<FrameImage> frameImage) {
        this.frameImages = frameImage;
    }
    public int getCurrentFrame() {
        return currentFrame;
    }
    public void setCurrentFrame(int currentFrame) {
        if (currentFrame > 0 && currentFrame < frameImages.size()) {
            this.currentFrame = currentFrame;
        } else {
            this.currentFrame = 0;
        }
    }
    public ArrayList<Boolean> getIgnoreFrame() {
        return ignoreFrame;
    }
    public void setIgnoreFrame(ArrayList<Boolean> ignoreFrame) {
        this.ignoreFrame = ignoreFrame;
    }
    public ArrayList<Double> getDelayTime() {
        return delayTime;
    }
    public void setDelayTime(ArrayList<Double> delayTime) {
        this.delayTime = delayTime;
    }
    public long getBeginTime() {
        return beginTime;
    }
    public void setBeginTime(long beginTime) {
        this.beginTime = beginTime;
    }
    public boolean getDrawRectFrame() {
        return drawRectFrame;
    }
    public void setDrawRectFrame(boolean drawRectFrame) {
        this.drawRectFrame = drawRectFrame;
    }

    //ham reset lai animation, khi frame chay toi cuoi, frame se duoc reset va chay lai tu dau.
    public void reset() {
        beginTime = 0;
        currentFrame = 0;

        for (int i=0; i<ignoreFrame.size(); i++) {
            ignoreFrame.set(i, false);
        }
    }

    /**
     * them 1 frameImage vao trong ArrayList frameImages. Va them khoang thoi gian delay cho frame do
     * @param frameImage frameImage can them
     * @param timeToNextFrame khoang thoi gian delay cho frame do
     */
    public void add(FrameImage frameImage, double timeToNextFrame) {
        frameImages.add(frameImage);
        ignoreFrame.add(false);
        delayTime.add(timeToNextFrame);
    }

    /**
     *ham lay hinh anh dang duoc in ra man hinh tai thoi diem hien tai
     * @return
     */
    BufferedImage getCurrentImage() {
        return frameImages.get(currentFrame).getImage();
    }

    /**
     * ham update frame dang duoc in ra man hinh
     * @param currentTime
     */
    public void update(long currentTime) {
        if (beginTime == 0) beginTime = currentTime;
        else {
            if (currentTime - beginTime > delayTime.get(currentFrame)) {
                nextFrame();
                beginTime = currentTime;
            }
        }
    }

    /**
     * ham chuyen toi frame tiep theo sau frame currentFrame
     */
    public void nextFrame() {
        if (currentFrame >= frameImages.size()-1) {
            if(isRepeated) {
                currentFrame = 0;
            }
        } else {
            currentFrame++;
        }
        if (ignoreFrame.get(currentFrame)) {
            nextFrame();
        }
    }

    /**
     * ham kiem tra xem frame Hien tai co phai frame cuoi cung khong
     * @return
     */
    public boolean isLastFrame() {
        if (currentFrame == frameImages.size()-1) return true;
        else return false;
    }

    /**
     * ham ve ra image tai toa do x, y truyen vao.
     * @param x toa do x muon in ra
     * @param y toa do y muon in ra
     * @param g2 graphics de in ra hinh anh
     */
    public void draw(int x, int y, Graphics2D g2) {
        float percent = 200/100;
        BufferedImage image = getCurrentImage();
        //BufferedImage resize = resize(image, (int)(image.getHeight()*percent), (int)(image.getWidth()*percent));
        //g2.drawImage(resize, x - image.getWidth()/2, y - image.getHeight()/2, null);
        g2.drawImage(image, x , y , null);

        if (drawRectFrame) {
            g2.drawRect(x - image.getWidth()/2, y - image.getHeight()/2, image.getWidth(), image.getHeight());
        }
    }

    public FrameImage getFrameImageAt(int i) {
        return frameImages.get(i);
    }

    public void drawLastFrame(int x, int y, Graphics2D g2) {
        BufferedImage image = getFrameImage().get(getFrameImage().size()-1).getImage();
        //BufferedImage resize = resize(image, (int)(image.getHeight()*percent), (int)(image.getWidth()*percent));
        g2.drawImage(image, x , y , null);
    }

}