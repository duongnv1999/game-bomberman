/**
 * @author : Nguyen Van duong - Nguyen Trong Hoang
 * @since : 2-12-2018
 */

package oop.uet.effect;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

/**
 * class co chuc nang la load toan bo data ve game va luu tru de su dung
 */

public class CacheDataLoader {
    private static CacheDataLoader instance = null;
    private Hashtable <String, FrameImage> frameImages;
    private Hashtable <String, Animation> animations;
    private String framefile, imageFile, animationFile,levelMap;

    private char[][] physicalMap;

    /**
     * constructor mac dinh voi muc truy cap private de dam bao lop chi co 1 the hien duy nhat la instace
     */
    private CacheDataLoader () {
        framefile ="Data/textures/frameImage.txt";
        imageFile = "Data/textures/sprite.png";
        animationFile = "Data/textures/animation.txt";
        levelMap = "Data/levels/Level1.txt";
    }

    public static CacheDataLoader getInstance () {
        if (instance == null) {
            instance = new CacheDataLoader();
        }
        return instance;
    }

    /**
     *
     */
    public void loadData() {
        try {
            LoadFrame();
            LoadAnimation();
            loadPhysicalMap();
        } catch (IOException e) {}
    }

    public char[][] getPhysicalMap () {
        return instance.physicalMap;
    }

    public FrameImage getFrameImage(String name) {
        FrameImage frameImage = new FrameImage(instance.frameImages.get(name));
        return frameImage;
    }

    public Animation getAnimation(String name) {
        Animation animation = new Animation(instance.animations.get(name));
        return animation;
    }

    private BufferedImage resize(BufferedImage img, int height, int width) {
        Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }


    /**
     *
     * @throws IOException
     */
    public void LoadFrame() throws IOException{

        frameImages = new Hashtable<String, FrameImage>();

        // mo file framefile
        FileReader fr = new FileReader(framefile);
        BufferedReader br = new BufferedReader(fr);

        // mo file classic.png
        BufferedImage bigImage = ImageIO.read(new File (imageFile));

        String line = br.readLine();

        if( line==null ) {
            System.out.println("No data");
            throw new IOException();
        } else {
            while (line != null) {
                while(line.equals("")) {
                    line = br.readLine();
                }

                String[] splString = line.trim().split("\\s+");
                int size = Integer.parseInt(splString[1]);
                int x = Integer.parseInt(splString[2]);
                int y = Integer.parseInt(splString[3]);
                int w = Integer.parseInt(splString[5]);
                int h = Integer.parseInt(splString[6]);
                System.out.println(splString[0]);

                BufferedImage subImage = bigImage.getSubimage(size*x, size*y, w, h);

                if (splString.length == 8) {
                    subImage = resize(subImage, 30, 30);
                } else {
                    subImage = resize(subImage, 40, 40);
                }
                FrameImage frame = new FrameImage(splString[0], subImage);

                instance.frameImages.put(splString[0], frame);

                line = br.readLine();
            }

        }
        br.close();
    }

    /**
     *
     * @throws IOException
     */
    public void LoadAnimation () throws IOException {
        animations = new Hashtable<String, Animation>();

        File file = new File (animationFile);
        BufferedReader br = new BufferedReader( new FileReader(file));

        String line = br.readLine();
        if (line == null) {
            System.out.println("no data");
        } else {
            while (line != null) {

                if (line.equals("")) {
                    line = br.readLine();
                }

                Animation animation = new Animation();
                String[] splString = line.trim().split("\\s+");
                for (int i=1; i<splString.length-1; i+=2) {
                    int timeToNextFrame = Integer.parseInt(splString[i+1]);
                    FrameImage frameImage = instance.getFrameImage(splString[i]);
                    animation.add(frameImage, timeToNextFrame);
                }
                instance.animations.put(splString[0], animation);
                line = br.readLine();
            }
        }

    }

    /**
     *
     * @throws IOException
     */
    public void loadPhysicalMap() throws IOException {

        File file = new File(levelMap);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String line = br.readLine();
        String[] s = line.trim().split("\\s+");
        int level = Integer.parseInt(s[0]);

        int numberOfRows = Integer.parseInt(s[1]);
        int numberOfColumns = Integer.parseInt(s[2]);
        instance.physicalMap = new char[numberOfRows][numberOfColumns];
        for (int r = 0; r<numberOfRows; r++) {
            line = br.readLine();
            for (int c=0; c<numberOfColumns; c++) {
                instance.physicalMap[r][c] = line.charAt(c);
            }
        }

        for (int r = 0; r<numberOfRows; r++) {
            for (int c=0; c<numberOfColumns; c++) {
                System.out.print(instance.physicalMap[r][c]);
            }
            System.out.println("");
        }
        br.close();

    }


}

